import React from 'react';

import imagen from '../imagenes/pizza-155771.svg';

class Anuncios extends React.Component{
    render(){
        return (
            <div>
                <h3>Sponsored</h3>
                <img className="alinear" src={imagen} alt="First slide" width="200" height="200"/>
            </div>
        );
    }

}

export default Anuncios;