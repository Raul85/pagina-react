import React from 'react';

import './styles/BarraNavegacion.css';
import { Link } from 'react-router-dom';

//iconos solidos
import { faHome,
         faPlayCircle,
         faStore,
         faUsers,
         faCube  } from '@fortawesome/free-solid-svg-icons';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class BarraNavegacion extends React.Component{
    render() {
        return (
            <div className="contenedor-Barra"> 
                <Link to="/"><FontAwesomeIcon icon={faHome} size="2x"/></Link>
                <Link to="/"><FontAwesomeIcon icon={faPlayCircle} size="2x"/></Link>
                <Link to="/"><FontAwesomeIcon icon={faStore} size="2x"/></Link>
                <Link to="/"><FontAwesomeIcon icon={faUsers} size="2x"/></Link>
                <Link to="/"><FontAwesomeIcon icon={faCube} size="2x"/></Link>
                
            </div>
        );
    }
}

export default BarraNavegacion;