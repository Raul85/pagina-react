import React from 'react';

import './styles/Layout.css';

//importando componentes
import Navbar from './Navbar';
import Menu from './MenuVertical';
import Carrusel from './Carrusel';
import Anuncios from './Anuncios';


class Plantilla extends React.Component{
    render() {
        return(
            <React.Fragment>
            <div>
                <Navbar />
                <div className="contenedor">
                    <div className="columna1">
                        <Menu />
                    </div>
                    <div className="columna2">
                        <Carrusel />
                    </div>
                    <div className="columna3">
                       <Anuncios />
                    </div>
                
                
                </div>
                    
            </div>
            </React.Fragment>
        );
    }
}

export default Plantilla;