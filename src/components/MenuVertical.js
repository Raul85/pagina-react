
import React from 'react';

//iconos solidos
import { faCog, 
         faChalkboardTeacher, 
         faCalendar,faBookmark, 
         faFlag,
         faUserFriends,
         faChessBoard } from '@fortawesome/free-solid-svg-icons';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Link } from 'react-router-dom';

import './styles/Menu.css';

class MenuVertical extends React.Component{
    render(){
        return(
            <div className="contenedor-menu">
                
                <ul className="menu">
                    <li><Link to="/"><h3>Home</h3></Link></li>
                    <li><Link to="/">Pending friend requests</Link></li>
                    
                    <li><Link to="/"><FontAwesomeIcon icon={faChalkboardTeacher} size="2x" className="margen" />Red Table Talk Group</Link></li>
                    <li><Link to="/"><FontAwesomeIcon icon={faCalendar} size="2x" className="margen" />Events</Link></li>
                    <li><Link to="/"><FontAwesomeIcon icon={faBookmark} size="2x" className="margen" />Saved</Link></li>
                    <li><Link to="/"><FontAwesomeIcon icon={faFlag} size="2x" className="margen" />Pages</Link></li>
                    <li><Link to="/"><FontAwesomeIcon icon={faUserFriends} size="2x" className="margen" />Friends</Link></li>
                    <li><Link to="/"><FontAwesomeIcon icon={faCog} size="2x" className="margen" />Settings & Privacy</Link></li>
                    <li><Link to="/"><FontAwesomeIcon icon={faChessBoard} size="2x" className="margen" />See More</Link></li>
                </ul>
            </div>
        )
    }
}

export default MenuVertical;