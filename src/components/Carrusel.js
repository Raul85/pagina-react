import React from 'react';

import { Link } from 'react-router-dom';

//imagenes de prueba para carrusel, luego se creara para que consuma API
import primeraImagen from '../imagenes/film-147137.svg';
import segundaImagen from '../imagenes/youtube-1684601.svg';

class Carrusel extends React.Component{
    render(){
        return (
            <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <img className="d-block w-100" src={primeraImagen} alt="First slide" width="200" height="200"/>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src={segundaImagen} alt="Second slide" width="200" height="200"/>
                        </div>
                        
                    </div>
                    <Link className="carousel-control-prev" to="#carouselExampleControls" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="sr-only">Atras</span>
                    </Link>
                    <Link className="carousel-control-next" to="#carouselExampleControls" role="button" data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="sr-only">Siguiente</span>
                    </Link>
            </div>
        );
    }
}

export default Carrusel;