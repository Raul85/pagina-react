import React from 'react';

import './styles/Navbar.css';
import Barra from './BarraNavegacion';

class Navbar extends React.Component{
    render() {
        return (
            <div className="Navbar">
                <div>
                    <Barra />
                </div>
                
            </div>
        );
    }
}

export default Navbar;