import React from 'react';

import Layout from '../components/Plantilla';

class Home extends React.Component{
    render() {
        return(
            <React.Fragment>
                <Layout />
            </React.Fragment>
        );
    }
}

export default Home;